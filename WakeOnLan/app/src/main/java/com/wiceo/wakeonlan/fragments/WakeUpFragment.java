package com.wiceo.wakeonlan.fragments;

import android.content.Context;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.wiceo.wakeonlan.MainActivity;
import com.wiceo.wakeonlan.R;
import com.wiceo.wakeonlan.adapters.DBAdapter;
import com.wiceo.wakeonlan.dragndrop_expandable_listview.DragNDropExLvAdapter;
import com.wiceo.wakeonlan.dragndrop_expandable_listview.DragNDropExLvListView;
import com.wiceo.wakeonlan.interfaces.OnDataPass;
import com.wiceo.wakeonlan.types.Computer;
import com.wiceo.wakeonlan.types.Group;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;


/**
 * Created by Pavel on 23.06.2015.
 */
public class WakeUpFragment extends Fragment {
    private Context context;
    private DragNDropExLvListView listview;
    public DragNDropExLvAdapter lv_adapter;
    private DBAdapter dbAdapter = MainActivity.dbAdapter;

    private OnDataPass dataPasser;

    public static WakeUpFragment newInstance() {
        WakeUpFragment f = new WakeUpFragment();

        //Bundle args = new Bundle();
        //args.putInt("userId", userId);
        //f.setArguments(args);

        return f;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);


    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.wake_up, container, false);

        context = getActivity().getApplicationContext();

        listview = (DragNDropExLvListView) view.findViewById(R.id.wake_up_list_view);
        listview.setDragOnLongPress(true);



        lv_adapter = new DragNDropExLvAdapter(getFragmentManager(), getActivity(), getGroups());
        listview.setAdapter(lv_adapter);

        return view;
    }

    private ArrayList<Group> getGroups(){
        ArrayList<Group> groups = dbAdapter.getAllGroups();
        Collections.sort(groups, new GroupComparator());
        return groups;
    }

    @Override
    public void setUserVisibleHint(boolean isVisibleToUser) {
        super.setUserVisibleHint(isVisibleToUser);

        // Make sure that we are currently visible
        if (this.isVisible()) {
            // If we are becoming invisible, then...
            if (!isVisibleToUser) {
                ArrayList<Group> groups = lv_adapter.getAllItems();
                for (Group g : groups){
                    for (Computer c : g.getComputers()){
                        dbAdapter.updateComputer(c);
                    }
                }
            } else {
                lv_adapter = new DragNDropExLvAdapter(getFragmentManager(), getActivity(), getGroups());
                listview.setAdapter(lv_adapter);
            }
        }
    }

    public class GroupComparator implements Comparator<Group> {
        @Override
        public int compare(Group u1, Group u2) {
            return u1.getPosition() - u2.getPosition();
        }
    }

    @Override
    public void onDestroy(){
        super.onDestroy();
        ArrayList<Group> groups = lv_adapter.getAllItems();
        for (Group g : groups){
            for (Computer c : g.getComputers()){
                dbAdapter.updateComputer(c);
            }
        }
    }


}
