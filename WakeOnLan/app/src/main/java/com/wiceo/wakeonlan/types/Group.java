package com.wiceo.wakeonlan.types;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;

/**
 * Created by Pavel on 23.06.2015.
 */
public class Group {
    private long id;
    private String name;
    private int position;
    private ArrayList<Computer> computers;

    public Group(){
        computers = new ArrayList<Computer>();
    }

    public Group(long id, String name, int position, ArrayList<Computer> computers){
        this.id = id;
        this.name = name;
        this.position = position;
        this.computers = computers;
        if (this.getComputers() != null) {
            Collections.sort(this.computers, new ComputerComparator());
        }
    }

    public void setId(long id) {
        this.id = id;
    }

    public long getId() {
        return id;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getName() {
        return name;
    }

    public void setPosition(int position){
        this.position = position;
    }

    public int getPosition() {
        return position;
    }

    public ArrayList<Computer> getComputers() {
        return computers;
    }

    public void setComputers(ArrayList<Computer> computers) {
        this.computers = computers;
        Collections.sort(this.computers, new ComputerComparator());
    }

    public void addComputer(int index, Computer computer){
        computers.add(index, computer);
    }

    public void addComputer(Computer computer){
        computers.add(computer);
    }

    public void deleteComputer(int index){
        computers.remove(index);
    }

    public void deleteComputer(Computer computer){
        computers.remove(computer);
    }

    public class ComputerComparator implements Comparator<Computer> {
        @Override
        public int compare(Computer u1, Computer u2) {
            return u1.getPosition() - u2.getPosition();
        }
    }
}
