package com.wiceo.wakeonlan.dragndrop_expandable_listview;

/*
 * Copyright (C) 2012 Sreekumar SH
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;

import com.wiceo.wakeonlan.ApplicationContextProvider;
import com.wiceo.wakeonlan.Constants;
import com.wiceo.wakeonlan.MagicPacketTask;
import com.wiceo.wakeonlan.MainActivity;
import com.wiceo.wakeonlan.R;
import com.wiceo.wakeonlan.dialogs.CustomDialog;
import com.wiceo.wakeonlan.dialogs.RenameGroup;
import com.wiceo.wakeonlan.interfaces.OnDataPass;
import com.wiceo.wakeonlan.interfaces.RenameGroupClick;
import com.wiceo.wakeonlan.types.Computer;
import com.wiceo.wakeonlan.types.Group;

import android.content.Context;
import android.graphics.PorterDuff;
import android.support.v4.app.FragmentActivity;
import android.support.v7.widget.PopupMenu;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseExpandableListAdapter;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.TextView;


/**
 * Adapter for the drag and drop listview
 *
 * @author <a href="http://sreekumar.sh" >Sreekumar SH </a>
 *         (sreekumar.sh@gmail.com)
 *
 */
public final class DragNDropExLvAdapter extends BaseExpandableListAdapter implements RenameGroupClick{


    private int selectedGroup;
    private int selectedChild;
    private Context mContext;
    private LayoutInflater mInflater;
    private OnDataPass dataPasser;
    private android.support.v4.app.FragmentManager fm;

    private ArrayList<Group> groups;

    public DragNDropExLvAdapter(android.support.v4.app.FragmentManager fm, Context context, ArrayList<Group> groups) {
        init(context, groups);

        this.fm = fm;
        try {
            dataPasser = (OnDataPass) fm.findFragmentByTag("android:switcher:" + R.id.main_pager + ":" + Constants.NEW_COMPUTER_TAB);
        } catch (ClassCastException e) {
            throw new ClassCastException("Calling fragment must implement AddComputerInterface interface");
        }
    }

    private void init(Context context,
                      ArrayList<Group> groups) {
        // Cache the LayoutInflate to avoid asking for a new one each time.
        mInflater = LayoutInflater.from(context);
        this.groups = groups;
        mContext = context;


    }

    public void onPick(int[] position) {
        selectedGroup = position[0];
        selectedChild = position[1];
    }

    public void onDrop(int[] from, int[] to) {
        if (to[0] > groups.size() || to[0] < 0 || to[1] < 0)
            return;

        Computer pc = (Computer)getChild(selectedGroup, selectedChild);
        ((Group)getGroup(selectedGroup)).deleteComputer(pc);

        pc.setGroup(getGroupId(to[0]));
        pc.setPosition(to[1]);
        ((Group)getGroup(to[0])).addComputer(to[1], pc);

        selectedGroup = -1;
        selectedChild = -1;
        onDropRewrite();
        notifyDataSetChanged();
    }

    @Override
    public Object getChild(int groupPosition, int childPosition) {
        // TODO Auto-generated method stub
        return (Computer) groups.get(groupPosition).getComputers().get(childPosition);
    }

    @Override
    public long getChildId(int groupPosition, int childPosition) {
        return (long) groups.get(groupPosition).getComputers().get(childPosition).getId();
    }

    public long getChildPosition(int groupPosition, int childPosition) {
        return childPosition;
    }

    public void onDropRewrite(){
        for (Group g : groups){
            int i = 0;
            for (Computer c : g.getComputers()){
                c.setGroup(g.getId());
                c.setPosition(i);
                i++;
            }
        }
    }

    @Override
    public View getChildView(final int groupPosition, final int childPosition,
                             boolean isLastChild, View convertView, ViewGroup parent) {

        final ComputerHolder holder;
        if (convertView == null) {
            convertView = mInflater.inflate(R.layout.dragndrop_row_item, null);

            holder = new ComputerHolder();

            holder.pc = (Computer)getChild(groupPosition, childPosition);
            holder.pcName = (TextView) convertView.findViewById(R.id.pc_name);
            holder.lastUsed = (TextView) convertView.findViewById(R.id.last_used);
            holder.pcIcon = (ImageView) convertView.findViewById(R.id.wake_up_launch);
            holder.pcModify = (ImageButton) convertView.findViewById(R.id.modify);
            convertView.setTag(holder);

        } else {
            holder = (ComputerHolder) convertView.getTag();
        }


        // Bind the data efficiently with the holder.
        holder.pc = (Computer)getChild(groupPosition, childPosition);
        holder.pcName.setText(holder.pc.getHostname());
        holder.pcIcon.setColorFilter(holder.pc.getColor(), PorterDuff.Mode.SRC_ATOP);
        holder.pcModify.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dataPasser.onDataPass(holder.pc);
                MainActivity.pager.setCurrentItem(Constants.NEW_COMPUTER_TAB);
            }
        });

        holder.pcIcon.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                //MagicPacketTask magicPacket = new MagicPacketTask();
                //magicPacket.execute(holder.pc);
                new Thread(new MagicPacketTask(holder.pc)).start();

                holder.pc.setLastUsed(new Date());
                groups.get(groupPosition).getComputers().get(childPosition).setLastUsed(new Date());

                DateFormat df = new SimpleDateFormat("MM.dd.yyyy HH:mm");
                Date date = holder.pc.getLastUsed();
                String reportDate = df.format(date);
                holder.lastUsed.setText(ApplicationContextProvider.getContext().getString(R.string.last_used) + " " + reportDate);

                return false;
            }
        });

        if (holder.pc.getUTLastUsed() != 0) {
            DateFormat df = new SimpleDateFormat("MM.dd.yyyy HH:mm");
            Date date = holder.pc.getLastUsed();
            String reportDate = df.format(date);
            holder.lastUsed.setText(ApplicationContextProvider.getContext().getString(R.string.last_used) + " " + reportDate);
        } else {
            holder.lastUsed.setText(ApplicationContextProvider.getContext().getString(R.string.last_used) + " " + ApplicationContextProvider.getContext().getString(R.string.never));
        }
        return convertView;
    }

    @Override
    public void groupRenamed(boolean renamed, int position, String name) {
        if (renamed){
            groups.get(position).setName(name);
            MainActivity.dbAdapter.updateGroup(groups.get(position));
            notifyDataSetChanged();
        }
    }


    static class ComputerHolder {
        TextView pcName;
        TextView lastUsed;
        ImageView pcIcon;
        ImageButton pcModify;
        Computer pc;
    }

    @Override
    public int getChildrenCount(int groupPosition) {
        return groups.get(groupPosition).getComputers().size();

    }

    @Override
    public Object getGroup(int groupPosition) {
        return groups.get(groupPosition);
    }


    @Override
    public int getGroupCount() {
        return groups.size();
    }

    @Override
    public long getGroupId(int groupPosition) {
        return (long) groups.get(groupPosition).getId();
    }

    public long getGroupPosition(int groupPosition){
        return groupPosition;
    }

    @Override
    public View getGroupView(final int groupPosition, boolean isExpanded,
                             View convertView, ViewGroup parent) {
        GroupHolder holder;
        if (convertView == null) {
            convertView = mInflater.inflate(R.layout.dragndrop_group_item, null);

            holder = new GroupHolder();
            holder.group = (Group)getGroup(groupPosition);
            holder.grName = (TextView) convertView.findViewById(R.id.group_name);
            holder.grModify = (ImageButton) convertView.findViewById(R.id.button_modify_group);

            convertView.setTag(holder);
        } else {
            holder = (GroupHolder) convertView.getTag();
        }

        // Bind the data efficiently with the holder.
        holder.grName.setText(holder.group.getName() + " (" + Integer.toString(getChildrenCount(groupPosition)) + ")");
        final ImageButton button = holder.grModify;
        final Group group = holder.group;
        holder.grModify.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                PopupMenu popup = new PopupMenu(mContext, button);
                //Inflating the Popup using xml file
                popup.getMenuInflater()
                        .inflate(R.menu.group_popup, popup.getMenu());

                //registering popup with OnMenuItemClickListener
                popup.setOnMenuItemClickListener(new PopupMenu.OnMenuItemClickListener() {
                    public boolean onMenuItemClick(MenuItem item) {

                        switch (item.getItemId()){
                            case R.id.rename_group:
                                RenameGroup dialog_fr = RenameGroup.newInstance(groupPosition);
                                dialog_fr.setTargetFragment(fm.findFragmentByTag("android:switcher:" + R.id.main_pager + ":" + MainActivity.pager.getCurrentItem()), 0);
                                dialog_fr.show(((FragmentActivity)mContext).getSupportFragmentManager(), "dialog_add");
                                break;
                            case R.id.delete_group:
                                final CustomDialog con_dialog = new CustomDialog();

                                con_dialog.setRightButton(ApplicationContextProvider.getContext().getResources().getString(R.string.delete), new View.OnClickListener() {
                                    @Override
                                    public void onClick(View v) {
                                        MainActivity.dbAdapter.deleteGroup(group);
                                        con_dialog.dismiss();
                                        groups.remove(groupPosition);
                                        notifyDataSetChanged();

                                    }
                                });

                                con_dialog.setRightButtonColor(ApplicationContextProvider.getContext().getResources().getColor(R.color.delete));
                                con_dialog.setLeftButton(ApplicationContextProvider.getContext().getResources().getString(R.string.cancel), new View.OnClickListener() {
                                    @Override
                                    public void onClick(View v) {
                                        con_dialog.dismiss();
                                    }
                                });

                                con_dialog.setLeftButtonColor(ApplicationContextProvider.getContext().getResources().getColor(R.color.primary));
                                con_dialog.setText(ApplicationContextProvider.getContext().getResources().getString(R.string.delete_group_message));
                                con_dialog.show(fm, "dialog_confirm");
                                break;

                        }

                        return true;
                    }
                });

                popup.show(); //showing popup menu
            }
        });


        ((DragNDropExLvListView) parent).expandGroup(groupPosition);

        return convertView;
    }

    static class GroupHolder {
        TextView grName;
        ImageButton grModify;
        Group group;
    }

    @Override
    public boolean hasStableIds() {
        return true;
    }

    @Override
    public boolean isChildSelectable(int groupPosition, int childPosition) {
        return true;
    }

    public ArrayList<Group> getAllItems(){
        return this.groups;
    }
}