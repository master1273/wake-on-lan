package com.wiceo.wakeonlan.adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import com.wiceo.wakeonlan.R;
import com.wiceo.wakeonlan.types.Group;

import java.util.ArrayList;

/**
 * Created by Master on 23.06.2015.
 */
public class GroupSpinnerAdapter extends ArrayAdapter<Group> {

    private Context context;
    private ArrayList<Group> groups;
    private int viewId;
    private int tvId;


    public GroupSpinnerAdapter(Context context, ArrayList<Group> groups, int viewId, int tvId) {
        super(context, viewId, groups);
        this.context = context;
        this.groups = groups;
        this.viewId = viewId;
        this.tvId = tvId;
    }

    /*private view holder class*/
    private class ViewHolder {
        TextView txtTitle;
    }

    @Override
    public View getDropDownView(int position, View convertView, ViewGroup parent){
        if (convertView == null)
        {
            LayoutInflater vi = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            convertView = vi.inflate(R.layout.custom_spinner_dropdown_item, null);
        }

        TextView textView = (TextView) convertView.findViewById(R.id.ab_sp_dp_i_tv);
        textView.setText(groups.get(position).getName());

        /*if (position  == 0) {
            textView.setHeight(0);
        }
        else{
            textView.setHeight(100);
        }*/

        return convertView;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        LayoutInflater inflater = (LayoutInflater) context
                .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        View rowView = convertView;

        if(convertView == null) {
            rowView = inflater.inflate(viewId, parent, false);
        }
        VHolder holder = new VHolder();

        holder.group = groups.get(position);
        holder.text = (TextView) rowView.findViewById(tvId);

        holder.text.setTag(holder.group);
        rowView.setTag(holder.group);

        setupItem(holder);



        return rowView;
    }

    private void setupItem(final VHolder holder){
        holder.text.setText(holder.group.getName());
    }

    public static class VHolder {
        Group group;
        TextView text;
    }

    public Group getItem(int position){

        return groups.get(position);
    }
}
