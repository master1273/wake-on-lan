package com.wiceo.wakeonlan.interfaces;

/**
 * Created by Master on 26.06.2015.
 */
public interface RenameGroupClick {
    public void groupRenamed(boolean renamed, int position, String name);
}
