package com.wiceo.wakeonlan.types;

/**
 * Created by Pavel on 23.06.2015.
 */
public class Widget {
    private long id;
    private Computer computer;

    public Widget (long id, Computer c){
        this.id = id;
        this.computer = c;
    }

    public void setId(long id) {
        this.id = id;
    }

    public void setComputer(Computer computer) {
        this.computer = computer;
    }

    public Computer getComputer() {
        return computer;
    }

    public long getId() {
        return id;
    }
}
