package com.wiceo.wakeonlan;

import android.content.Intent;
import android.content.SharedPreferences;
import android.net.Uri;
import android.os.Handler;
import android.os.StrictMode;
import android.provider.Settings;
import android.support.annotation.Nullable;
import android.support.v4.view.ViewPager;
import android.support.v7.app.ActionBarActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.text.Html;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.RelativeLayout;
import android.widget.Toast;

import com.astuetz.PagerSlidingTabStrip;
import com.google.android.gms.ads.AdSize;
import com.google.android.vending.licensing.AESObfuscator;
import com.google.android.vending.licensing.LicenseChecker;
import com.google.android.vending.licensing.LicenseCheckerCallback;
import com.google.android.vending.licensing.Policy;
import com.google.android.vending.licensing.ServerManagedPolicy;
import com.wiceo.wakeonlan.adapters.DBAdapter;
import com.wiceo.wakeonlan.adapters.MyPagerAdapter;
import com.wiceo.wakeonlan.dialogs.CustomDialog;
import com.wiceo.wakeonlan.util.IabHelper;
import com.wiceo.wakeonlan.util.IabResult;
import com.google.android.gms.ads.AdRequest;
import com.google.android.gms.ads.AdView;
import com.wiceo.wakeonlan.R;
import com.wiceo.wakeonlan.util.Inventory;
import com.wiceo.wakeonlan.util.Purchase;


public class MainActivity extends ActionBarActivity {

    public static DBAdapter dbAdapter;
    public static ViewPager pager;
    private SharedPreferences sPrefs;
    private AdView mAdView;
    boolean mIsPremium = false;

    static final String SKU_PREMIUM = "com.wiceo.wakeonlan";
    static final int REQUEST_CODE = 505;

    private boolean doubleBackToExitPressedOnce;
    private Handler backHandler = new Handler();
    private final Runnable backRunnable = new Runnable() {
        @Override
        public void run() {
            doubleBackToExitPressedOnce = false;
        }
    };

    private static final String TAG1 = "com.wiceo.wakeonlan";
    IabHelper mHelper;


    private Handler mHandler;

    IabHelper.OnIabPurchaseFinishedListener mPurchaseFinishedListener
            = new IabHelper.OnIabPurchaseFinishedListener() {
        public void onIabPurchaseFinished(IabResult result,
                                          Purchase purchase)
        {
            if (result.isFailure()) {
                Log.d(TAG, "Error: " + result);
                Toast.makeText(ApplicationContextProvider.getContext(), "Error: " + result.getMessage(), Toast.LENGTH_SHORT ).show();
                return;
            }
            else if (purchase.getSku().equals(SKU_PREMIUM)) {
                //mIsPremium = true;
                //Intent starterIntent = MainActivity.this.getIntent();
                //finish();
                //startActivity(starterIntent);
                MainActivity.this.recreate();
            }

        }
    };

    private static final String TAG = "inappbilling";

    @Override
    protected void onStart() {
        super.onStart();


    }

    IabHelper.QueryInventoryFinishedListener mReceivedInventoryListener
            = new IabHelper.QueryInventoryFinishedListener() {
        public void onQueryInventoryFinished(IabResult result,
                                             Inventory inventory) {


            if (result.isFailure()) {
                Toast.makeText(ApplicationContextProvider.getContext(), "Error receiving purchased items", Toast.LENGTH_SHORT).show();
                Log.d(TAG, "Error receiving purchased items");
            } else {
                mHelper.consumeAsync(inventory.getPurchase(SKU_PREMIUM),
                        mConsumeFinishedListener);
            }
        }
    };

    IabHelper.OnConsumeFinishedListener mConsumeFinishedListener =
            new IabHelper.OnConsumeFinishedListener() {
                public void onConsumeFinished(Purchase purchase,
                                              IabResult result) {

                    if (result.isSuccess()) {
                        mIsPremium = true;
                    } else {
                        Toast.makeText(ApplicationContextProvider.getContext(), "Error verifying purchased item", Toast.LENGTH_SHORT).show();
                        Log.d(TAG, "Error verifying purchased item");
                    }
                }
            };

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        String base64EncodedPublicKey = ApplicationContextProvider.getContext().getResources().getString(R.string.billing_id);

        mHelper = new IabHelper(this, base64EncodedPublicKey);

        mHelper.startSetup(new IabHelper.OnIabSetupFinishedListener() {
            public void onIabSetupFinished(IabResult result) {
                if (!result.isSuccess()) {
                    Log.d(TAG, "Billing setup failed: " + result);
                    Toast.makeText(ApplicationContextProvider.getContext(), "Billing setup failed: " + result.getMessage(), Toast.LENGTH_SHORT).show();
                    return;
                }

                Toast.makeText(ApplicationContextProvider.getContext(), "Setup successful. Querying inventory.", Toast.LENGTH_SHORT).show();
                Log.d(TAG, "Setup successful. Querying inventory.");
                mHelper.queryInventoryAsync(mReceivedInventoryListener);
            }
        });

        if (mIsPremium){
            setContentView(R.layout.activity_main_purchased);
        } else {
            setContentView(R.layout.activity_main);
        }


        dbAdapter = new DBAdapter(this);
        dbAdapter.open();

        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setTitle(getResources().getString(R.string.app_name));

        // Initialize the ViewPager and set an adapter
        pager = (ViewPager) findViewById(R.id.main_pager);
        pager.setAdapter(new MyPagerAdapter(getSupportFragmentManager()));

        // Bind the tabs to the ViewPager
        PagerSlidingTabStrip tabs = (PagerSlidingTabStrip) findViewById(R.id.main_tabs);
        tabs.setShouldExpand(true);
        tabs.setViewPager(pager);
        tabs.setTextColor(getResources().getColor(R.color.white));

        sPrefs = getPreferences(MODE_PRIVATE);
        if(sPrefs.getBoolean("first_launch", true) || dbAdapter.getAllGroups().size() == 0){
            pager.setCurrentItem(Constants.NEW_COMPUTER_TAB);
            SharedPreferences.Editor ed = sPrefs.edit();
            ed.putBoolean("first_launch", false);
            ed.commit();
        } else {
            pager.setCurrentItem(Constants.WAKE_UP_TAB);
        }

        mAdView = (AdView) this.findViewById(R.id.ad_view);
        if (mAdView!= null) {
            AdRequest adRequest = new AdRequest.Builder()
                    .addTestDevice(AdRequest.DEVICE_ID_EMULATOR)
                    .addTestDevice("TEST_DEVICE_ID")
                    .build();

            // Start loading the ad in the background.
            mAdView.loadAd(adRequest);
        }



    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        if (mIsPremium){
            getMenuInflater().inflate(R.menu.menu_main_purchased, menu);
        } else {
            getMenuInflater().inflate(R.menu.menu_main, menu);
        }
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()){
            case R.id.instruction:
                final CustomDialog con_dialog = new CustomDialog();

                con_dialog.setRightButton(ApplicationContextProvider.getContext().getResources().getString(R.string.ok), new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        con_dialog.dismiss();

                    }
                });

                con_dialog.setRightButtonColor(ApplicationContextProvider.getContext().getResources().getColor(R.color.primary));
                con_dialog.setLeftButton(null, null);

                con_dialog.setText(getText(R.string.instruction_info));
                con_dialog.show(getSupportFragmentManager(), "dialog_instructions");
                return true;
            case R.id.about:
                final CustomDialog about_dialog = new CustomDialog();

                about_dialog.setRightButton(ApplicationContextProvider.getContext().getResources().getString(R.string.ok), new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        about_dialog.dismiss();

                    }
                });

                about_dialog.setRightButtonColor(ApplicationContextProvider.getContext().getResources().getColor(R.color.primary));
                about_dialog.setLeftButton(null, null);

                about_dialog.setText(Html.fromHtml(ApplicationContextProvider.getContext().getResources().getString(R.string.about_info)));
                about_dialog.enableLinks();
                about_dialog.show(getSupportFragmentManager(), "about_dialog");
                return true;
            case R.id.rate_app:
                Intent intent = new Intent(Intent.ACTION_VIEW);
                intent.setData(Uri.parse("market://details?id=com.wiceo.wakeonlan"));
                startActivity(intent);
                return true;
            case R.id.remove_ads:
                mHelper.launchPurchaseFlow(this, SKU_PREMIUM, REQUEST_CODE, mPurchaseFinishedListener);
                return true;
        }

        return super.onOptionsItemSelected(item);
    }



    @Override
    protected void onDestroy() {
        super.onDestroy();
        if (dbAdapter != null)
            dbAdapter.close();

        if (mHelper != null)
            try {
                mHelper.dispose();
            }catch (IllegalArgumentException ex){
                ex.printStackTrace();
            }finally{}
        mHelper = null;

        if (backHandler != null){
            mHandler.removeCallbacks(backRunnable);
        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode,
                                    Intent data)
    {
        if (!mHelper.handleActivityResult(requestCode,
                resultCode, data)) {
            super.onActivityResult(requestCode, resultCode, data);
        }
    }

    @Override
    public void onBackPressed() {
        if (doubleBackToExitPressedOnce) {
            super.onBackPressed();
            return;
        }

        this.doubleBackToExitPressedOnce = true;
        Toast.makeText(this, ApplicationContextProvider.getContext().getResources().getString(R.string.back_message), Toast.LENGTH_SHORT).show();

        mHandler.postDelayed(backRunnable, 2000);

    }

}
