package com.wiceo.wakeonlan.types;

import android.graphics.Color;

import java.io.Serializable;
import java.util.Date;

/**
 * Created by Pavel on 23.06.2015.
 */
public class Computer implements Serializable {

    private long id;
    private String hostname;
    private String mac;
    private String ip;
    private int port;
    private long lastUsed;
    private int color;
    private long groupId;
    private int position;

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public void setHostname(String hostname) {
        this.hostname = hostname;
    }


    public void setColor(int color) {
        this.color = color;
    }

    public void setGroup(long groupId) {
        this.groupId = groupId;
    }

    public void setIp(String ip) {
        this.ip = ip;
    }

    public void setMac(String mac) {
        this.mac = mac;
    }

    public void setPort(int port) {
        this.port = port;
    }

    public void setUTLastUsed(long lastUsed) {
        this.lastUsed = lastUsed;
    }

    public void setLastUsed(Date lastUsed) {
        this.lastUsed = lastUsed.getTime();
    }

    public int getColor() {
        return color;
    }

    public long getGroup() {
        return groupId;
    }

    public int getPort() {
        return port;
    }

    public long getUTLastUsed() {
        return lastUsed;
    }

    public Date getLastUsed() {
        return new Date(lastUsed);
    }

    public String getHostname() {
        return hostname;
    }

    public String getIp() {
        return ip;
    }

    public String getMac() {
        return mac;
    }


    public int getPosition() {
        return position;
    }

    public void setPosition(int position) {
        this.position = position;
    }
}
