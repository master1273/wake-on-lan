package com.wiceo.wakeonlan.interfaces;

import com.wiceo.wakeonlan.types.Group;

/**
 * Created by Master on 25.06.2015.
 */

public interface AddComputerInterface {
    public void addGroupButtonClick(Group group);
}