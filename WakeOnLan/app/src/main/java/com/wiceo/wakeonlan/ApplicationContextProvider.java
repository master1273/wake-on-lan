package com.wiceo.wakeonlan;

import android.app.Application;
import android.content.Context;

/**
 * Created by Master on 23.09.2014.
 */
public class ApplicationContextProvider extends Application {
    /**
     * Keeps a reference of the application context
     */
    private static Context sContext;

    @Override
    public void onCreate() {
        super.onCreate();
        sContext = getApplicationContext();
    }

    /**
     * Returns the application context
     * @return application context
     */
    public static Context getContext() {
        return sContext;
    }
}
