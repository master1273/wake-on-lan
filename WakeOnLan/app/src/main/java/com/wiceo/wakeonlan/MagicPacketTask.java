package com.wiceo.wakeonlan;

/**
 * Created by Master on 25.06.2015.
 */
import android.os.AsyncTask;
import android.os.Handler;
import android.os.Looper;
import android.util.Log;
import android.widget.Toast;

import com.wiceo.wakeonlan.types.Computer;

import java.io.IOException;

import java.net.InetAddress;
import java.net.UnknownHostException;
import java.net.DatagramSocket;
import java.net.DatagramPacket;
import java.net.SocketException;

import java.lang.IllegalArgumentException;
import java.lang.StringBuffer;

import java.util.regex.Pattern;
import java.util.regex.Matcher;


/**
 *	@desc	Static WOL magic packet class
 */

public class MagicPacketTask implements Runnable
{
    private Computer pc;

    public MagicPacketTask(Computer pc){
        this.pc = pc;
    }

    private byte[] getMacBytes(String mac) throws IllegalArgumentException {
        // TODO Auto-generated method stub

        mac = mac.replaceAll(":", "");

        byte[] bytes = new byte[6];
        if (mac.length() != 12)
        {
            throw new IllegalArgumentException("Invalid MAC address...");
        }
        try {
            String hex;
            for (int i = 0; i < 6; i++) {
                hex = mac.substring(i*2, i*2+2);
                bytes[i] = (byte) Integer.parseInt(hex, 16);
            }
        }
        catch (NumberFormatException e) {
            throw new IllegalArgumentException("Invalid hex digit...");
        }
        return bytes;
    }

    public void wakeUp(Computer pc) {
        if (pc.getMac() == null) {
            return;
        }

        try {
            byte[] macBytes = getMacBytes(pc.getMac());
            byte[] bytes = new byte[6 + 16 * macBytes.length];
            for (int i = 0; i < 6; i++) {
                bytes[i] = (byte) 0xff;
            }
            for (int i = 6; i < bytes.length; i += macBytes.length) {
                System.arraycopy(macBytes, 0, bytes, i, macBytes.length);
            }

            InetAddress address = InetAddress.getByName(pc.getIp());
            DatagramPacket packet = new DatagramPacket(bytes, bytes.length, address, pc.getPort());
            DatagramSocket socket = new DatagramSocket();
            socket.send(packet);
            socket.close();

            Runnable task = new Runnable() {
                @Override
                public void run() {
                    Toast.makeText(ApplicationContextProvider.getContext(), ApplicationContextProvider.getContext().getResources().getString(R.string.result_success), Toast.LENGTH_SHORT).show();
                    Thread.currentThread().interrupt();
                }
            };
            new Handler(Looper.getMainLooper()).post(task);
        }
        catch (SocketException e) {
            final String ex = e.getMessage();
            Runnable task = new Runnable() {
                @Override
                public void run() {
                    Toast.makeText(ApplicationContextProvider.getContext(), ApplicationContextProvider.getContext().getResources().getString(R.string.result_failed), Toast.LENGTH_SHORT).show();
                    Thread.currentThread().interrupt();
                }
            };
            new Handler(Looper.getMainLooper()).post(task);
        }
        catch (Exception e){
            Runnable task = new Runnable() {
                @Override
                public void run() {
                    Toast.makeText(ApplicationContextProvider.getContext(), ApplicationContextProvider.getContext().getResources().getString(R.string.result_failed), Toast.LENGTH_SHORT).show();
                    Thread.currentThread().interrupt();
                }
            };
            new Handler(Looper.getMainLooper()).post(task);
        }


    }

    @Override
    public void run() {
        wakeUp(pc);

    }
}
