package com.wiceo.wakeonlan.adapters;

import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;

import com.wiceo.wakeonlan.ApplicationContextProvider;
import com.wiceo.wakeonlan.R;
import com.wiceo.wakeonlan.fragments.AddNewComputerFragment;
import com.wiceo.wakeonlan.fragments.WakeUpFragment;

/**
 * Created by Pavel on 23.06.2015.
 */
public class MyPagerAdapter extends FragmentPagerAdapter {

    private final String[] TITLES = {ApplicationContextProvider.getContext().getResources().getString(R.string.wake_up),
            ApplicationContextProvider.getContext().getResources().getString(R.string.add_new_computer)};

    public MyPagerAdapter(FragmentManager fm) {
        super(fm);
    }

    @Override
    public CharSequence getPageTitle(int position) {
        return TITLES[position];
    }

    @Override
    public int getCount() {
        return TITLES.length;
    }

    @Override
    public Fragment getItem(int position) {

        switch (position) {
            case 0:
                return WakeUpFragment.newInstance();
            case 1:
                return AddNewComputerFragment.newInstance(null);
            default:
                return null;
        }
    }
}
