package com.wiceo.wakeonlan.dialogs;

/**
 * Created by Master on 27.02.2015.
 */

import android.support.v4.app.DialogFragment;
import android.content.DialogInterface;
import android.os.Bundle;
import android.text.method.LinkMovementMethod;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.TextView;

import com.wiceo.wakeonlan.R;
import com.gc.materialdesign.views.ButtonFlat;

public class CustomDialog extends DialogFragment implements OnClickListener {

    private String title;
    private String text;
    private CharSequence c_text;
    private String rightButtonText;
    private String leftButtonText;
    private View.OnClickListener onRightButtonClickListener;
    private View.OnClickListener onLeftButtonClickListener;
    private int rightButtonColor = 0;
    private int leftButtonColor = 0;

    private TextView title_tv;
    private TextView text_tv;
    private ButtonFlat rightButton;
    private ButtonFlat leftButton;

    public void setTitle(String title){
        this.title = title;

        if (title_tv != null) {
            if (title != null && !title.equals("") && !title.isEmpty()) {
                title_tv.setVisibility(View.VISIBLE);
                title_tv.setText(title);
            } else {
                title_tv.setVisibility(View.GONE);
            }
        }
    }

    public void setText(String text){
        this.text = text;

        if (text_tv != null) {
            if (text != null && !text.equals("") && !text.isEmpty()) {
                text_tv.setVisibility(View.VISIBLE);
                text_tv.setText(text);
            } else {
                text_tv.setVisibility(View.GONE);
            }
        }
    }

    public void setText(CharSequence text){
        this.c_text = text;

        if (text_tv != null) {
            if (c_text != null && !c_text.equals("") && c_text.length() > 0) {
                text_tv.setVisibility(View.VISIBLE);
                text_tv.setText(c_text);
            } else {
                text_tv.setVisibility(View.GONE);
            }
        }
    }

    public void setRightButton(String rightButtonText, View.OnClickListener onRightButtonClickListener){
        this.rightButtonText = rightButtonText;
        this.onRightButtonClickListener = onRightButtonClickListener;

        if (rightButton != null){
            if (!rightButtonText.equals("") && !rightButtonText.isEmpty()){
                rightButton.setVisibility(View.VISIBLE);
                rightButton.setText(rightButtonText);

                if(onRightButtonClickListener != null){
                    rightButton.setOnClickListener(onRightButtonClickListener);
                }

            } else {
                rightButton.setVisibility(View.GONE);
            }
        }
    }

    public void setRightButtonColor(int rightButtonColor){
        this.rightButtonColor = rightButtonColor;

        if(rightButtonColor != 0 && rightButton != null){
            rightButton.setTextColor(rightButtonColor);
        }
    }

    public void setLeftButton(String leftButtonText, OnClickListener onLeftButtonClickListener){
        this.leftButtonText = leftButtonText;
        this.onLeftButtonClickListener = onLeftButtonClickListener;

        if (leftButton != null) {
            if (leftButtonText != null && !leftButtonText.equals("") && !leftButtonText.isEmpty()) {
                leftButton.setVisibility(View.VISIBLE);
                leftButton.setText(leftButtonText);

                if (onLeftButtonClickListener != null) {
                    leftButton.setOnClickListener(onLeftButtonClickListener);
                }
            } else {
                leftButton.setVisibility(View.GONE);
            }
        }
    }

    public void setLeftButtonColor(int leftButtonColor){
        this.leftButtonColor = leftButtonColor;

        if(leftButtonColor != 0 && leftButton != null){
            leftButton.setTextColor(leftButtonColor );
        }

    }


    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        getDialog().getWindow().requestFeature(Window.FEATURE_NO_TITLE);
        View v = inflater.inflate(R.layout.custom_dialog, null);


        this.title_tv = (TextView) v.findViewById(R.id.dialog_title);
        setTitle(title);

        this.text_tv = (TextView) v.findViewById(R.id.dialog_text);

        if (text != null)
            setText(text);

        if (c_text != null)
            setText(c_text);

        this.rightButton = (ButtonFlat) v.findViewById(R.id.right_button);
        setRightButton(rightButtonText, onRightButtonClickListener);
        setRightButtonColor(rightButtonColor);

        this.leftButton = (ButtonFlat) v.findViewById(R.id.left_button);
        setLeftButton(leftButtonText, onLeftButtonClickListener);
        setLeftButtonColor(leftButtonColor);

        return v;
    }

    public void onClick(View v) {

    }

    public void onDismiss(DialogInterface dialog) {
        super.onDismiss(dialog);

    }

    public void onCancel(DialogInterface dialog) {
        super.onCancel(dialog);

    }

    public void enableLinks(){
        if (text_tv != null){
            text_tv.setLinksClickable(true);
            text_tv.setMovementMethod(new LinkMovementMethod());
        }
    }
}