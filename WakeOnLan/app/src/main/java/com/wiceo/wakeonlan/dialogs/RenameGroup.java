package com.wiceo.wakeonlan.dialogs;

import android.content.Context;
import android.content.DialogInterface;
import android.os.Bundle;
import android.support.v4.app.DialogFragment;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.inputmethod.InputMethodManager;
import android.widget.EditText;
import android.widget.TextView;

import com.gc.materialdesign.views.ButtonFlat;
import com.wiceo.wakeonlan.MainActivity;
import com.wiceo.wakeonlan.R;
import com.wiceo.wakeonlan.fragments.WakeUpFragment;
import com.wiceo.wakeonlan.interfaces.AddComputerInterface;
import com.wiceo.wakeonlan.interfaces.RenameGroupClick;
import com.wiceo.wakeonlan.types.Group;

/**
 * Created by Master on 26.06.2015.
 */
public class RenameGroup extends DialogFragment implements View.OnClickListener {

    private RenameGroupClick callback;
    private EditText name;
    private ButtonFlat confirm;

    public static RenameGroup newInstance(int position) {
        RenameGroup f = new RenameGroup();

        Bundle args = new Bundle();
        args.putInt("position", position);
        f.setArguments(args);

        return f;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        try {
            callback = (RenameGroupClick) ((WakeUpFragment)getFragmentManager().findFragmentByTag("android:switcher:" + R.id.main_pager + ":" + MainActivity.pager.getCurrentItem())).lv_adapter;
        } catch (ClassCastException e) {
            dismiss();
            //throw new ClassCastException("Calling fragment must implement AddComputerInterface interface");
        }
    }

    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        getDialog().getWindow().requestFeature(Window.FEATURE_NO_TITLE);
        View v = inflater.inflate(R.layout.edit_group, null);

        name = (EditText) v.findViewById(R.id.edit_group_edit);
        name.setOnEditorActionListener(new TextView.OnEditorActionListener() {

            @Override
            public boolean onEditorAction(TextView v, int actionId,
                                          KeyEvent event) {
                if (event != null && (event.getKeyCode() == KeyEvent.KEYCODE_ENTER)) {

                    InputMethodManager in = (InputMethodManager) getActivity().getSystemService(Context.INPUT_METHOD_SERVICE);

                    in.hideSoftInputFromWindow(name
                                    .getApplicationWindowToken(),
                            InputMethodManager.HIDE_NOT_ALWAYS);

                    return true;
                }
                return false;
            }
        });
        confirm = (ButtonFlat) v.findViewById(R.id.edit_group_button);
        confirm.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                callback.groupRenamed(true, getArguments().getInt("position"), name.getText().toString());
                dismiss();
            }
        });


        return v;
    }

    @Override
    public void onDismiss(DialogInterface dialog) {
        super.onDismiss(dialog);
        name.setText("");
    }

    @Override
    public void onCancel(DialogInterface dialog) {
        super.onCancel(dialog);
        callback.groupRenamed(false, -1, "");
    }

    @Override
    public void onClick(View v) {
        dismiss();
    }
}