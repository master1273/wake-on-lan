package com.wiceo.wakeonlan.dialogs;

import android.support.v4.app.DialogFragment;
import android.content.DialogInterface;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.EditText;

import com.gc.materialdesign.views.ButtonFlat;
import com.wiceo.wakeonlan.MainActivity;
import com.wiceo.wakeonlan.R;
import com.wiceo.wakeonlan.fragments.AddNewComputerFragment;
import com.wiceo.wakeonlan.interfaces.AddComputerInterface;
import com.wiceo.wakeonlan.types.Group;


/**
 * Created by Master on 25.06.2015.
 */


public class AddNewGroup extends DialogFragment implements OnClickListener {

    private AddComputerInterface callback;
    private EditText name;
    private ButtonFlat confirm;
    private Group group;

    public static AddNewGroup newInstance(int position) {
        AddNewGroup f = new AddNewGroup();

        Bundle args = new Bundle();
        args.putInt("position", position);
        f.setArguments(args);

        return f;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        try {
            callback = (AddComputerInterface) getFragmentManager().findFragmentByTag("android:switcher:" + R.id.main_pager + ":" + MainActivity.pager.getCurrentItem());
        } catch (ClassCastException e) {
            dismiss();
            //throw new ClassCastException("Calling fragment must implement AddComputerInterface interface");
        }
    }

    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        getDialog().getWindow().requestFeature(Window.FEATURE_NO_TITLE);
        View v = inflater.inflate(R.layout.add_new_group, null);

        group = new Group();
        group.setPosition(getArguments().getInt("position"));

        name = (EditText) v.findViewById(R.id.add_new_group_edit);
        confirm = (ButtonFlat) v.findViewById(R.id.add_new_group_button);
        confirm.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                group.setName(name.getText().toString());
                callback.addGroupButtonClick(group);
                dismiss();
            }
        });


        return v;
    }

    @Override
    public void onDismiss(DialogInterface dialog) {
        super.onDismiss(dialog);
        name.setText("");
    }

    @Override
    public void onCancel(DialogInterface dialog) {
        super.onCancel(dialog);
        callback.addGroupButtonClick(null);
    }

    @Override
    public void onClick(View v) {
        dismiss();
    }
}
