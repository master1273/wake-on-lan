package com.wiceo.wakeonlan.adapters;

/**
 * Created by Pavel on 23.06.2015.
 */
import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.SQLException;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

import com.wiceo.wakeonlan.ApplicationContextProvider;
import com.wiceo.wakeonlan.R;
import com.wiceo.wakeonlan.types.Computer;
import com.wiceo.wakeonlan.types.Group;
import com.wiceo.wakeonlan.types.Widget;

import java.util.ArrayList;

/**
 * Created by Master on 21.02.2015.
 */
public class DBAdapter {

    public static final int DB_VERSION = 1;
    public static final String DB_NAME = "wol_db";

    private static final String COMPUTERS_TABLE = "computers";

    private static final String ROW_ID = "_id";
    private static final String HOSTNAME = "hostname";
    private static final String MAC = "mac";
    private static final String IP = "ip";
    private static final String PORT = "port";
    private static final String LAST_USED = "last_used";
    private static final String COLOR = "color";
    private static final String GROUP = "group_id";
    private static final String COMPUTER_POSITION = "computer_position";

    private static final String COMPUTERS_GROUPS = "groups";
    private static final String GROUP_NAME = "group_name";
    private static final String GROUP_POSITION = "group_positon";

    private static final String WIDGET = "widget";
    private static final String DEVICE_ID = "device_id";


    private final Context context;
    private DatabaseHelper DBHelper;
    private SQLiteDatabase db;

    public DBAdapter(Context ctx)
    {
        this.context = ctx;
        this.DBHelper = new DatabaseHelper(this.context);
    }

    private static class DatabaseHelper extends SQLiteOpenHelper {

        DatabaseHelper(Context context)
        {
            super(context, DB_NAME, null, DB_VERSION);
        }

        @Override
        public void onCreate(SQLiteDatabase db) {
            final String CREATE_TABLE_COMPUTERS = "CREATE TABLE " + COMPUTERS_TABLE
                    + "("+ ROW_ID+" INTEGER PRIMARY KEY AUTOINCREMENT, "
                    + HOSTNAME + " TEXT, "
                    + MAC + " TEXT, "
                    + IP + " TEXT, "
                    + PORT + " INTEGER, "
                    + LAST_USED + " INTEGER, "
                    + COLOR + " INTEGER, "
                    + GROUP + " INTEGER, "
                    + COMPUTER_POSITION + " INTEGER);";
            db.execSQL(CREATE_TABLE_COMPUTERS);

            final String CREATE_TABLE_GROUPS = "CREATE TABLE " + COMPUTERS_GROUPS
                    + "("+ ROW_ID+" INTEGER PRIMARY KEY AUTOINCREMENT, "
                    + GROUP_NAME + " TEXT, "
                    + GROUP_POSITION + " INTEGER);";
            db.execSQL(CREATE_TABLE_GROUPS);

            final String CREATE_TABLE_WIDGET = "CREATE TABLE " + WIDGET
                    + "("+ ROW_ID+" INTEGER PRIMARY KEY AUTOINCREMENT, "
                    + DEVICE_ID + " INTEGER);";
            db.execSQL(CREATE_TABLE_WIDGET);

            /*ContentValues initialValues = new ContentValues();
            initialValues.put(GROUP_NAME, ApplicationContextProvider.getContext().getResources().getString(R.string.first_group_name));
            initialValues.put(GROUP_POSITION, 0);

            db.insert(COMPUTERS_GROUPS, null, initialValues);*/
        }

        @Override
        public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {

        }
    }

    public DBAdapter open() throws SQLException
    {
        this.db = this.DBHelper.getWritableDatabase();
        return this;
    }

    public void close()
    {
        this.DBHelper.close();
    }

    /**
     *
     * GROUP METHODS
     *
     */

    public long createGroup(Group group){
        ContentValues initialValues = new ContentValues();
        initialValues.put(GROUP_NAME, group.getName());
        initialValues.put(GROUP_POSITION, group.getPosition());

        return this.db.insert(COMPUTERS_GROUPS, null, initialValues);
    }

    public boolean deleteGroup(Group g) {
        for (Computer c : g.getComputers()){
            deleteComputer(c.getId());
        }
        return this.db.delete(COMPUTERS_GROUPS, ROW_ID + "=" + g.getId(), null) > 0; //$NON-NLS-1$
    }

    public ArrayList<Group> getAllGroups() {

        ArrayList<Group> list = new ArrayList<Group>();

        Cursor mCursor =  this.db.query(COMPUTERS_GROUPS, new String[] { ROW_ID, GROUP_NAME, GROUP_POSITION },
                null, null, null, null, null);

        if (mCursor != null){
            mCursor.moveToFirst();
            while (!mCursor.isAfterLast()){
                Group g = new Group(mCursor.getLong(mCursor.getColumnIndex(ROW_ID)),
                        mCursor.getString(mCursor.getColumnIndex(GROUP_NAME)),
                        mCursor.getInt(mCursor.getColumnIndex(GROUP_POSITION)),
                        getGroupComputers(mCursor.getLong(mCursor.getColumnIndex(ROW_ID))));
                list.add(g);
                mCursor.moveToNext();
            }
            mCursor.close();
        }
        return list;
    }

    public Group getGroup(long rowId) throws SQLException {

        Cursor mCursor = this.db.query(COMPUTERS_GROUPS, new String[] { ROW_ID, GROUP_NAME, GROUP_POSITION },
                ROW_ID + "=" + rowId, null, null, null, null, null);
        Group g = null;
        if (mCursor != null) {
            mCursor.moveToFirst();
            g = new Group(mCursor.getLong(mCursor.getColumnIndex(ROW_ID)),
                    mCursor.getString(mCursor.getColumnIndex(GROUP_NAME)),
                    mCursor.getInt(mCursor.getColumnIndex(GROUP_POSITION)),
                    getGroupComputers(mCursor.getLong(mCursor.getColumnIndex(ROW_ID))));
            mCursor.close();
        }
        return g;
    }

    public boolean updateGroup(Group g){
        ContentValues initialValues = new ContentValues();
        initialValues.put(GROUP_NAME, g.getName());
        initialValues.put(GROUP_POSITION, g.getPosition());
        for (Computer c : g.getComputers()){
            updateComputer(c);
        }

        return this.db.update(COMPUTERS_GROUPS, initialValues, ROW_ID + "=" + g.getId(), null) >0;
    }

    /***
     *
     * COMPUTER METHODS
     *
     */

    public long createComputer(Computer c){
        ContentValues initialValues = new ContentValues();
        initialValues.put(HOSTNAME, c.getHostname());
        initialValues.put(MAC, c.getMac());
        initialValues.put(IP, c.getIp());
        initialValues.put(PORT, c.getPort());
        initialValues.put(LAST_USED, c.getUTLastUsed());
        initialValues.put(COLOR, c.getColor());
        initialValues.put(GROUP, c.getGroup());
        initialValues.put(COMPUTER_POSITION, c.getPosition());
        return this.db.insert(COMPUTERS_TABLE, null, initialValues);
    }

    public boolean deleteComputer(long rowId) {

        return this.db.delete(COMPUTERS_TABLE, ROW_ID + "=" + rowId, null) > 0; //$NON-NLS-1$
    }

    public ArrayList<Computer> getAllComputers() {

        ArrayList<Computer> list = new ArrayList<Computer>();

        Cursor mCursor =  this.db.query(COMPUTERS_TABLE, new String[] { ROW_ID,
                HOSTNAME, MAC, IP, PORT, LAST_USED, COLOR, GROUP, COMPUTER_POSITION},
                null, null, null, null, null);

        if (mCursor != null){
            mCursor.moveToFirst();
            while (!mCursor.isAfterLast()){
                Computer c = new Computer();

                c.setId(mCursor.getInt(mCursor.getColumnIndex(ROW_ID)));
                c.setHostname(mCursor.getString(mCursor.getColumnIndex(HOSTNAME)));
                c.setMac(mCursor.getString(mCursor.getColumnIndex(MAC)));
                c.setIp(mCursor.getString(mCursor.getColumnIndex(IP)));
                c.setPort(mCursor.getInt(mCursor.getColumnIndex(PORT)));
                c.setUTLastUsed(mCursor.getLong(mCursor.getColumnIndex(LAST_USED)));
                c.setColor(mCursor.getInt(mCursor.getColumnIndex(COLOR)));
                c.setGroup(mCursor.getLong(mCursor.getColumnIndex(GROUP)));
                c.setPosition(mCursor.getInt(mCursor.getColumnIndex(COMPUTER_POSITION)));

                list.add(c);
                mCursor.moveToNext();
            }
            mCursor.close();
        }
        return list;
    }

    public ArrayList<Computer> getGroupComputers(long groupId) throws SQLException {

        ArrayList<Computer> list = new ArrayList<Computer>();

        Cursor mCursor =  this.db.query(COMPUTERS_TABLE, new String[] { ROW_ID,
                        HOSTNAME, MAC, IP, PORT, LAST_USED, COLOR, GROUP, COMPUTER_POSITION},
                GROUP + "=" + groupId, null, null, null, null);

        if (mCursor != null){
            mCursor.moveToFirst();
            while (!mCursor.isAfterLast()){
                Computer c = new Computer();

                c.setId(mCursor.getInt(mCursor.getColumnIndex(ROW_ID)));
                c.setHostname(mCursor.getString(mCursor.getColumnIndex(HOSTNAME)));
                c.setMac(mCursor.getString(mCursor.getColumnIndex(MAC)));
                c.setIp(mCursor.getString(mCursor.getColumnIndex(IP)));
                c.setPort(mCursor.getInt(mCursor.getColumnIndex(PORT)));
                c.setUTLastUsed(mCursor.getLong(mCursor.getColumnIndex(LAST_USED)));
                c.setColor(mCursor.getInt(mCursor.getColumnIndex(COLOR)));
                c.setGroup(mCursor.getLong(mCursor.getColumnIndex(GROUP)));
                c.setPosition(mCursor.getInt(mCursor.getColumnIndex(COMPUTER_POSITION)));

                list.add(c);
                mCursor.moveToNext();
            }
            mCursor.close();
        }
        return list;
    }

    public Computer getComputer(long rowId) throws SQLException {

        Cursor mCursor = this.db.query(COMPUTERS_TABLE, new String[] { ROW_ID,
                HOSTNAME, MAC, IP, PORT, LAST_USED, COLOR, GROUP, COMPUTER_POSITION},
                ROW_ID + "=" + rowId, null, null, null, null, null);
        Computer c = null;
        if (mCursor != null) {
            mCursor.moveToFirst();
            c = new Computer();
            c.setId(mCursor.getInt(mCursor.getColumnIndex(ROW_ID)));
            c.setHostname(mCursor.getString(mCursor.getColumnIndex(HOSTNAME)));
            c.setMac(mCursor.getString(mCursor.getColumnIndex(MAC)));
            c.setIp(mCursor.getString(mCursor.getColumnIndex(IP)));
            c.setPort(mCursor.getInt(mCursor.getColumnIndex(PORT)));
            c.setUTLastUsed(mCursor.getLong(mCursor.getColumnIndex(LAST_USED)));
            c.setColor(mCursor.getInt(mCursor.getColumnIndex(COLOR)));
            c.setGroup(mCursor.getLong(mCursor.getColumnIndex(GROUP)));
            c.setPosition(mCursor.getInt(mCursor.getColumnIndex(COMPUTER_POSITION)));

            mCursor.close();
        }
        return c;
    }

    public boolean updateComputer(Computer c){
        ContentValues initialValues = new ContentValues();
        initialValues.put(HOSTNAME, c.getHostname());
        initialValues.put(MAC, c.getMac());
        initialValues.put(IP, c.getIp());
        initialValues.put(LAST_USED, c.getUTLastUsed());
        initialValues.put(COLOR, c.getColor());
        initialValues.put(GROUP, c.getGroup());
        initialValues.put(COMPUTER_POSITION, c.getPosition());

        return this.db.update(COMPUTERS_TABLE, initialValues, ROW_ID + "=" + c.getId(), null) >0;
    }

    /**
     *
     * WIDGET METHODS
     *
     */

    public long createWidget(Widget w){
        ContentValues initialValues = new ContentValues();
        initialValues.put(DEVICE_ID, w.getComputer().getId());

        return this.db.insert(WIDGET, null, initialValues);
    }

    public boolean deleteWidget(long rowId) {

        return this.db.delete(WIDGET, ROW_ID + "=" + rowId, null) > 0; //$NON-NLS-1$
    }

    public ArrayList<Widget> getAllWidgets() {

        ArrayList<Widget> list = new ArrayList<Widget>();

        Cursor mCursor =  this.db.query(WIDGET, new String[] { ROW_ID, DEVICE_ID },
                null, null, null, null, null);

        if (mCursor != null){
            mCursor.moveToFirst();
            while (!mCursor.isAfterLast()){
                Widget w = new Widget(mCursor.getLong(mCursor.getColumnIndex(ROW_ID)),
                        getComputer(mCursor.getLong(mCursor.getColumnIndex(DEVICE_ID))));
                list.add(w);
                mCursor.moveToNext();
            }
            mCursor.close();
        }
        return list;
    }

    public Widget getWidget(long rowId) throws SQLException {

        Cursor mCursor = this.db.query(WIDGET, new String[] { ROW_ID, DEVICE_ID },
                ROW_ID + "=" + rowId, null, null, null, null, null);
        Widget w = null;
        if (mCursor != null) {
            mCursor.moveToFirst();
            w = new Widget(mCursor.getLong(mCursor.getColumnIndex(ROW_ID)),
                    getComputer(mCursor.getLong(mCursor.getColumnIndex(DEVICE_ID))));
            mCursor.close();
        }
        return w;
    }

    public boolean updateWidget(Widget w){
        ContentValues initialValues = new ContentValues();
        initialValues.put(DEVICE_ID, w.getComputer().getId());

        return this.db.update(WIDGET, initialValues, ROW_ID + "=" + w.getId(), null) >0;
    }
}
