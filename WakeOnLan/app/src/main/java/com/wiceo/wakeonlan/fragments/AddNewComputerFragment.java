package com.wiceo.wakeonlan.fragments;

import android.annotation.SuppressLint;
import android.content.Context;
import android.content.DialogInterface;
import android.graphics.Color;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.InputMethodManager;
import android.widget.AdapterView;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.gc.materialdesign.views.ButtonFlat;
import com.gc.materialdesign.views.ButtonRectangle;
import com.gc.materialdesign.widgets.ColorSelector;
import com.wiceo.wakeonlan.ApplicationContextProvider;
import com.wiceo.wakeonlan.MainActivity;
import com.wiceo.wakeonlan.NDSpinner;
import com.wiceo.wakeonlan.R;
import com.wiceo.wakeonlan.adapters.DBAdapter;
import com.wiceo.wakeonlan.adapters.GroupSpinnerAdapter;
import com.wiceo.wakeonlan.dialogs.AddNewGroup;
import com.wiceo.wakeonlan.dialogs.CustomDialog;
import com.wiceo.wakeonlan.interfaces.AddComputerInterface;
import com.wiceo.wakeonlan.interfaces.OnDataPass;
import com.wiceo.wakeonlan.types.Computer;
import com.wiceo.wakeonlan.types.Group;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.regex.Matcher;
import java.util.regex.Pattern;


/**
 * Created by Pavel on 23.06.2015.
 */
public class AddNewComputerFragment extends Fragment implements AddComputerInterface, OnDataPass {

    private Context context;
    private NDSpinner groupSpinner;
    private GroupSpinnerAdapter spinnerAdapter;
    private DBAdapter dbAdapter = MainActivity.dbAdapter;
    private Computer computer = null;
    private int previousSelected;
    private int groupSize;
    private boolean newComputer, visible;

    private ButtonRectangle colorBtn;
    private EditText mNameEdit;
    private EditText mIpEdit;
    private EditText mPort;
    private EditText mMacEdit;
    private ButtonFlat delete;
    private TextView label;



    public static AddNewComputerFragment newInstance(Computer computer) {
        AddNewComputerFragment f = new AddNewComputerFragment();

        Bundle args = new Bundle();
        args.putSerializable("computer", computer);
        f.setArguments(args);

        return f;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        newComputer = true;
        computer = null;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.add_new_computer, container, false);

        context = getActivity().getApplicationContext();

        groupSpinner = (NDSpinner) view.findViewById(R.id.group_spinner);


        label = (TextView) view.findViewById(R.id.add_new_pc_tv);

        mNameEdit = (EditText) view.findViewById(R.id.pc_name);
        mNameEdit.setOnEditorActionListener(new TextView.OnEditorActionListener() {

            @Override
            public boolean onEditorAction(TextView v, int actionId,
                                          KeyEvent event) {
                if (event != null && (event.getKeyCode() == KeyEvent.KEYCODE_ENTER)) {

                    InputMethodManager in = (InputMethodManager) getActivity().getSystemService(Context.INPUT_METHOD_SERVICE);

                    in.hideSoftInputFromWindow(mNameEdit
                                    .getApplicationWindowToken(),
                            InputMethodManager.HIDE_NOT_ALWAYS);

                    return true;
                }
                return false;
            }
        });

        mIpEdit = (EditText) view.findViewById(R.id.pc_ip);
        mIpEdit.setOnEditorActionListener(new TextView.OnEditorActionListener() {

            @Override
            public boolean onEditorAction(TextView v, int actionId,
                                          KeyEvent event) {
                if (event != null && (event.getKeyCode() == KeyEvent.KEYCODE_ENTER)) {

                    if (mPort.getText().toString().equals("") || mPort.getText().toString() == null) {
                        setFocus(mPort);
                    } else {

                        InputMethodManager in = (InputMethodManager) getActivity().getSystemService(Context.INPUT_METHOD_SERVICE);

                        in.hideSoftInputFromWindow(mIpEdit
                                        .getApplicationWindowToken(),
                                InputMethodManager.HIDE_NOT_ALWAYS);
                    }
                    final Pattern IP_ADDRESS
                            = Pattern.compile(
                            "((25[0-5]|2[0-4][0-9]|[0-1][0-9]{2}|[1-9][0-9]|[1-9])\\.(25[0-5]|2[0-4]"
                                    + "[0-9]|[0-1][0-9]{2}|[1-9][0-9]|[1-9]|0)\\.(25[0-5]|2[0-4][0-9]|[0-1]"
                                    + "[0-9]{2}|[1-9][0-9]|[1-9]|0)\\.(25[0-5]|2[0-4][0-9]|[0-1][0-9]{2}"
                                    + "|[1-9][0-9]|[0-9]))");
                    Matcher matcher = IP_ADDRESS.matcher(mIpEdit.getText());
                    if (!matcher.matches()) {
                        Toast.makeText(context, ApplicationContextProvider.getContext().getResources().getString(R.string.incorrect_ip), Toast.LENGTH_SHORT).show();
                    }

                    return true;
                }
                return false;
            }
        });
        mIpEdit.setOnFocusChangeListener(new View.OnFocusChangeListener() {
            @Override
            public void onFocusChange(View v, boolean hasFocus) {
                if (!hasFocus) {
                    final Pattern IP_ADDRESS
                            = Pattern.compile(
                            "((25[0-5]|2[0-4][0-9]|[0-1][0-9]{2}|[1-9][0-9]|[1-9])\\.(25[0-5]|2[0-4]"
                                    + "[0-9]|[0-1][0-9]{2}|[1-9][0-9]|[1-9]|0)\\.(25[0-5]|2[0-4][0-9]|[0-1]"
                                    + "[0-9]{2}|[1-9][0-9]|[1-9]|0)\\.(25[0-5]|2[0-4][0-9]|[0-1][0-9]{2}"
                                    + "|[1-9][0-9]|[0-9]))");
                    Matcher matcher = IP_ADDRESS.matcher(mIpEdit.getText());
                    if (!matcher.matches() && !mIpEdit.getText().toString().equals("")) {
                        Toast.makeText(context, ApplicationContextProvider.getContext().getResources().getString(R.string.incorrect_ip), Toast.LENGTH_SHORT).show();
                    }
                }
            }
        });

        mPort = (EditText) view.findViewById(R.id.pc_port);
        mPort.setOnEditorActionListener(new TextView.OnEditorActionListener() {

            @Override
            public boolean onEditorAction(TextView v, int actionId,
                                          KeyEvent event) {
                if (event != null && (event.getKeyCode() == KeyEvent.KEYCODE_ENTER)) {

                        InputMethodManager in = (InputMethodManager) getActivity().getSystemService(Context.INPUT_METHOD_SERVICE);

                        in.hideSoftInputFromWindow(mPort
                                        .getApplicationWindowToken(),
                                InputMethodManager.HIDE_NOT_ALWAYS);

                    return true;
                }
                return false;
            }
        });

        mMacEdit = (EditText) view.findViewById(R.id.pc_mac);
        mMacEdit.addTextChangedListener(new TextWatcher() {
            String mPreviousMac = null;

            @Override
            public void afterTextChanged(Editable arg0) {
            }

            @Override
            public void beforeTextChanged(CharSequence arg0, int arg1, int arg2, int arg3) {
            }


            @SuppressLint("DefaultLocale")
            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                if (!mMacEdit.getText().toString().equals("")) {
                    String enteredMac = mMacEdit.getText().toString().toUpperCase();
                    String cleanMac = clearNonMacCharacters(enteredMac);
                    String formattedMac = formatMacAddress(cleanMac);

                    int selectionStart = mMacEdit.getSelectionStart();
                    formattedMac = handleColonDeletion(enteredMac, formattedMac, selectionStart);
                    int lengthDiff = formattedMac.length() - enteredMac.length();

                    setMacEdit(cleanMac, formattedMac, selectionStart, lengthDiff);
                }
            }

            private String clearNonMacCharacters(String mac) {
                return mac.toString().replaceAll("[^A-Fa-f0-9]", "");
            }

            private String formatMacAddress(String cleanMac) {
                int grouppedCharacters = 0;
                String formattedMac = "";

                for (int i = 0; i < cleanMac.length(); ++i) {
                    formattedMac += cleanMac.charAt(i);
                    ++grouppedCharacters;

                    if (grouppedCharacters == 2) {
                        formattedMac += ":";
                        grouppedCharacters = 0;
                    }
                }

                if (cleanMac.length() == 12)
                    formattedMac = formattedMac.substring(0, formattedMac.length() - 1);

                return formattedMac;
            }

            private String handleColonDeletion(String enteredMac, String formattedMac, int selectionStart) {
                if (mPreviousMac != null && mPreviousMac.length() > 1) {
                    int previousColonCount = colonCount(mPreviousMac);
                    int currentColonCount = colonCount(enteredMac);

                    if (currentColonCount < previousColonCount) {
                        formattedMac = formattedMac.substring(0, selectionStart - 1) + formattedMac.substring(selectionStart);
                        String cleanMac = clearNonMacCharacters(formattedMac);
                        formattedMac = formatMacAddress(cleanMac);
                    }
                }
                return formattedMac;
            }

            private int colonCount(String formattedMac) {
                return formattedMac.replaceAll("[^:]", "").length();
            }

            private void setMacEdit(String cleanMac, String formattedMac, int selectionStart, int lengthDiff) {
                mMacEdit.removeTextChangedListener(this);
                if (cleanMac.length() <= 12) {
                    mMacEdit.setText(formattedMac);
                    mMacEdit.setSelection(selectionStart + lengthDiff);
                    mPreviousMac = formattedMac;
                } else {
                    mMacEdit.setText(mPreviousMac);
                    mMacEdit.setSelection(mPreviousMac.length());
                }
                mMacEdit.addTextChangedListener(this);
            }
        });

        mMacEdit.setOnEditorActionListener(new TextView.OnEditorActionListener() {

            @Override
            public boolean onEditorAction(TextView v, int actionId,
                                          KeyEvent event) {
                if (event != null && (event.getKeyCode() == KeyEvent.KEYCODE_ENTER)) {
                    if (mIpEdit.getText().toString().equals("") || mIpEdit.getText().toString() == null) {
                        setFocus(mIpEdit);
                    } else {
                        InputMethodManager in = (InputMethodManager) getActivity().getSystemService(Context.INPUT_METHOD_SERVICE);

                        in.hideSoftInputFromWindow(mMacEdit
                                        .getApplicationWindowToken(),
                                InputMethodManager.HIDE_NOT_ALWAYS);
                    }
                    return true;
                }
                return false;
            }
        });

        delete = (ButtonFlat) view.findViewById(R.id.button_delete);

        colorBtn = (ButtonRectangle) view.findViewById(R.id.pick_color);
        colorBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ColorSelector colorSelector = new ColorSelector(getActivity(), computer.getColor(),
                        new ColorSelector.OnColorSelectedListener() {
                            @Override
                            public void onColorSelected(int i) {
                                colorBtn.setBackgroundColor(i);
                                computer.setColor(i);

                                double darkness = 1-(0.299*Color.red(i) + 0.587*Color.green(i) + 0.114*Color.blue(i))/255;
                                if(darkness<0.5){
                                    colorBtn.setTextColor(ApplicationContextProvider.getContext().getResources().getColor(R.color.primary_text));
                                }else{
                                    colorBtn.setTextColor(Color.WHITE);
                                }
                            }
                        });
                colorSelector.setOnShowListener(new DialogInterface.OnShowListener() {
                    @Override
                    public void onShow(DialogInterface dialog) {
                        Toast.makeText(ApplicationContextProvider.getContext(), ApplicationContextProvider.getContext().getResources().getString(R.string.press_back_to_apply_color), Toast.LENGTH_SHORT).show();
                    }
                });
                colorSelector.show();
            }
        });

        ButtonFlat save = (ButtonFlat) view.findViewById(R.id.button_save);
        save.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                final Pattern IP_ADDRESS
                        = Pattern.compile(
                        "((25[0-5]|2[0-4][0-9]|[0-1][0-9]{2}|[1-9][0-9]|[1-9])\\.(25[0-5]|2[0-4]"
                                + "[0-9]|[0-1][0-9]{2}|[1-9][0-9]|[1-9]|0)\\.(25[0-5]|2[0-4][0-9]|[0-1]"
                                + "[0-9]{2}|[1-9][0-9]|[1-9]|0)\\.(25[0-5]|2[0-4][0-9]|[0-1][0-9]{2}"
                                + "|[1-9][0-9]|[0-9]))");
                Matcher matcher2 = IP_ADDRESS.matcher(mIpEdit.getText());
                if (!matcher2.matches()) {
                    Toast.makeText(context, ApplicationContextProvider.getContext().getResources().getString(R.string.incorrect_ip), Toast.LENGTH_SHORT).show();
                } else if (mMacEdit.getText().toString().length() != 17) {
                    Toast.makeText(context, ApplicationContextProvider.getContext().getResources().getString(R.string.incorrect_mac), Toast.LENGTH_SHORT).show();
                } else if (((Group) groupSpinner.getSelectedItem()).getId() == -1){
                    Toast.makeText(context, ApplicationContextProvider.getContext().getResources().getString(R.string.no_group), Toast.LENGTH_SHORT).show();
                } else {

                    int count = dbAdapter.getAllComputers().size();
                    Computer new_computer = new Computer();
                    if (!mNameEdit.getText().toString().equals("") ) {
                        computer.setHostname(mNameEdit.getText().toString());
                    } else {
                        computer.setHostname(ApplicationContextProvider.getContext().getResources().getString(R.string.computer) + Integer.toString(count + 1));
                    }
                    new_computer.setGroup(((Group) groupSpinner.getSelectedItem()).getId());
                    new_computer.setColor(computer.getColor());
                    new_computer.setIp(mIpEdit.getText().toString());
                    new_computer.setMac(mMacEdit.getText().toString());
                    if (mPort.getText().toString().equals("")){
                        new_computer.setPort(9);
                    } else {
                        new_computer.setPort(Integer.parseInt(mPort.getText().toString()));
                    }
                    new_computer.setUTLastUsed(computer.getUTLastUsed());

                    computer.setUTLastUsed(new_computer.getUTLastUsed());
                    computer.setIp(new_computer.getIp());
                    computer.setMac(new_computer.getMac());
                    if (computer.getGroup() != new_computer.getGroup()){
                        int groupPcCount = dbAdapter.getGroupComputers(new_computer.getGroup()).size();
                        computer.setPosition(groupPcCount);
                        computer.setGroup(new_computer.getGroup());
                    } else {
                        computer.setGroup(new_computer.getGroup());
                    }
                    computer.setPort(new_computer.getPort());
                    computer.setColor(new_computer.getColor());

                    if (computer.getId() == -200){
                        dbAdapter.createComputer(computer);
                    } else {
                        dbAdapter.updateComputer(computer);
                    }
                    gotoWakeUpTab();
                    clearFields();
                }
            }
        });

        visible = true;
        setupSpinner();
        //groupSpinner.setSelection(0);

        return view;
    }

    private void setupFields(){
        if (computer != null){
            newComputer = false;
            mNameEdit.setText(computer.getHostname());
            mIpEdit.setText(computer.getIp());
            mMacEdit.setText(computer.getMac());
            mPort.setText(Integer.toString(computer.getPort()));
            delete.setVisibility(View.VISIBLE);
            delete.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    final CustomDialog con_dialog = new CustomDialog();

                    con_dialog.setRightButton(ApplicationContextProvider.getContext().getResources().getString(R.string.delete), new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            dbAdapter.deleteComputer(computer.getId());
                            con_dialog.dismiss();
                            gotoWakeUpTab();
                            clearFields();

                        }
                    });

                    con_dialog.setRightButtonColor(ApplicationContextProvider.getContext().getResources().getColor(R.color.delete));
                    con_dialog.setLeftButton(ApplicationContextProvider.getContext().getResources().getString(R.string.cancel), new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            con_dialog.dismiss();
                        }
                    });

                    con_dialog.setLeftButtonColor(ApplicationContextProvider.getContext().getResources().getColor(R.color.primary));
                    con_dialog.setText(ApplicationContextProvider.getContext().getResources().getString(R.string.delete_message));
                    con_dialog.show(getFragmentManager(), "dialog_confirm");

                }
            });
            colorBtn.setBackgroundColor(computer.getColor());
            //computer.setColor(i);

            double darkness = 1-(0.299*Color.red(computer.getColor()) + 0.587*Color.green(computer.getColor()) + 0.114*Color.blue(computer.getColor()))/255;
            if(darkness<0.5){
                colorBtn.setTextColor(ApplicationContextProvider.getContext().getResources().getColor(R.color.primary_text));
            }else{
                colorBtn.setTextColor(Color.WHITE);
            }
        }else{
            newComputer = true;
            computer = new Computer();
            computer.setId(-200);
            computer.setGroup(-300);
            computer.setColor(ApplicationContextProvider.getContext().getResources().getColor(R.color.primary));
            computer.setUTLastUsed(0);
        }
    }

    private void setupSpinner(){


        if (spinnerAdapter != null){
            spinnerAdapter.clear();
        }

        ArrayList<Group> groups = new ArrayList<Group>();
        groups.addAll(dbAdapter.getAllGroups());
        groupSize = groups.size();
        Group group = new Group(-1, ApplicationContextProvider.getContext().getResources().getString(R.string.add_new), groups.size() ,null);
        groups.add(group);
        Collections.sort(groups, new GroupComparator());
        spinnerAdapter = new GroupSpinnerAdapter(context, groups, R.layout.custom_spinner_item, R.id.custom_spinner_item_tv);

        spinnerAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        groupSpinner.setAdapter(spinnerAdapter);
        groupSpinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                if(visible){
                    if (((Group) view.getTag()).getId() == -1) {
                        AddNewGroup dialog_fr = AddNewGroup.newInstance(groupSize);
                        dialog_fr.setTargetFragment(getParentFragment(), 0);
                        dialog_fr.show(getActivity().getSupportFragmentManager(), "dialog_add");
                    } else {
                        previousSelected = position;
                    }
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });

    }

    @Override
    public void addGroupButtonClick(Group group) {
        if (group != null) {
            dbAdapter.createGroup(group);
            setupSpinner();
            groupSpinner.setSelection(groupSize-1);
        } else {
            //groupSpinner.setSelection(previousSelected);
        }
    }

    @Override
    public void onDataPass(Computer computer) {
        this.computer = computer;
        newComputer = false;
        label.setText(ApplicationContextProvider.getContext().getResources().getString(R.string.edit_pc_label));
    }


    public class GroupComparator implements Comparator<Group> {
        @Override
        public int compare(Group u1, Group u2) {
            return u1.getPosition() - u2.getPosition();
        }
    }

    private void setFocus(EditText editText){
        editText.requestFocus();
        InputMethodManager imm = (InputMethodManager) getActivity().getSystemService(Context.INPUT_METHOD_SERVICE);
        imm.showSoftInput(editText, InputMethodManager.SHOW_IMPLICIT);
    }

    private void gotoWakeUpTab(){
        MainActivity.pager.setCurrentItem(0);
    }

    @Override
    public void setUserVisibleHint(boolean isVisibleToUser) {
        super.setUserVisibleHint(isVisibleToUser);
        if (this.isVisible()) {
            if (!isVisibleToUser) {
                visible = false;
                    clearFields();
                    computer = null;
                    //newComputer = true;
            } else {
                visible = true;
                setupSpinner();
                groupSpinner.setSelection(0);
                setupFields();
            }
        }
    }

    private void clearFields(){
        label.setText(ApplicationContextProvider.getContext().getResources().getString(R.string.add_new_pc_label));
        mNameEdit.setText("");
        mIpEdit.setText("");
        mPort.setText("");
        mMacEdit.setText("");
        delete.setVisibility(View.GONE);

        int i = ApplicationContextProvider.getContext().getResources().getColor(R.color.primary);

        colorBtn.setBackgroundColor(i);
        //computer.setColor(i);

        double darkness = 1-(0.299*Color.red(i) + 0.587*Color.green(i) + 0.114*Color.blue(i))/255;
        if(darkness<0.5){
            colorBtn.setTextColor(ApplicationContextProvider.getContext().getResources().getColor(R.color.primary_text));
        }else{
            colorBtn.setTextColor(Color.WHITE);
        }
    }

}
