package com.wiceo.wakeonlan.interfaces;

import com.wiceo.wakeonlan.types.Computer;

/**
 * Created by Master on 25.06.2015.
 */
public interface OnDataPass {
    public void onDataPass(Computer computer);
}